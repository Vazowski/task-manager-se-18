package ru.iteco.taskmanager.api;

import ru.iteco.taskmanager.api.endpoint.Session;

public interface ISessionService {

    void setSession(Session session);

    Session getSession();
}
