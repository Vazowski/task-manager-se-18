package ru.iteco.taskmanager.command.project.remove;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Singleton
public class ProjectRemoveAllCommand extends AbstractCommand {

    @Inject
    private IProjectEndpoint projectEndpoint;
    @Inject
    private ISessionService sessionService;

    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "  -  remove one project";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
        if (sessionDTO == null)
            return;
        projectEndpoint.removeAllProject(sessionDTO);
        System.out.println("Done");
    }
}
