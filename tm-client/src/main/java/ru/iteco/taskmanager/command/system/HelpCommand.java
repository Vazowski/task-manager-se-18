package ru.iteco.taskmanager.command.system;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.iteco.taskmanager.api.ITerminalService;
import ru.iteco.taskmanager.command.AbstractCommand;

@Singleton
public class HelpCommand extends AbstractCommand {

    @Inject
    private ITerminalService terminalService;

    @Override
    public String command() {
	return "help";
    }

    @Override
    public String description() {
	return "  -  show all commands";
    }

    @Override
    public void execute() throws Exception {
	System.out.println();
	for (final String key : terminalService.getCommands().keySet()) {
	    if (!key.equals("login") && !key.equals("login")) {
		System.out.println(key + terminalService.getCommands().get(key).description());
	    }
	}
    }
}
