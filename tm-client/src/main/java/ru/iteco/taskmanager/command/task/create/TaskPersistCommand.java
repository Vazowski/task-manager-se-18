package ru.iteco.taskmanager.command.task.create;

import java.util.Date;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Project;
import ru.iteco.taskmanager.api.endpoint.ReadinessStatus;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.api.endpoint.Task;
import ru.iteco.taskmanager.api.endpoint.TaskDTO;
import ru.iteco.taskmanager.api.endpoint.UserDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.DateUtil;
import ru.iteco.taskmanager.util.convert.ProjectDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.TaskDTOConvertUtil;

@Singleton
public class TaskPersistCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;
    @Inject
    private IProjectEndpoint projectEndpoint;
    @Inject
    private ITaskEndpoint taskEndpoint;
    @Inject
    private ISessionService sessionService;

    @Override
    public String command() {
	return "task-persist";
    }

    @Override
    public String description() {
	return "  -  persist task";
    }

    @Override
    public void execute() throws Exception {
	@Nullable
	final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
	if (sessionDTO == null)
	    return;
	@Nullable
	final UserDTO userDTO = userEndpoint.findUserById(sessionDTO, sessionDTO.getUserId());
	if (userDTO == null)
	    return;

	System.out.print("Name of project: ");
	@NotNull
	final String inputProjectName = scanner.nextLine();
	@Nullable
	final Project tempProject = ProjectDTOConvertUtil
		.DTOToProject(projectEndpoint.findProjectByName(sessionDTO, inputProjectName));
	if (tempProject == null) {
	    System.out.println("Project doesn't exist");
	    return;
	}
	System.out.print("Name of task: ");
	@NotNull
	final String inputName = scanner.nextLine();
	System.out.print("Description of task: ");
	@NotNull
	final String inputDescription = scanner.nextLine();
	System.out.print("Date of begining task: ");
	@NotNull
	final String dateBegin = scanner.nextLine();
	System.out.print("Date of ending task: ");
	@NotNull
	final String dateEnd = scanner.nextLine();
	@Nullable
	final Task tempTask = TaskDTOConvertUtil.DTOToTask(taskEndpoint.findTaskByName(sessionDTO, inputName));

	if (tempTask == null) {
	    String uuid = UUID.randomUUID().toString();
	    @Nullable
	    TaskDTO taskDTO = new TaskDTO();
	    taskDTO.setId(uuid);
	    taskDTO.setOwnerId(userDTO.getId());
	    taskDTO.setProjectId(tempProject.getId());
	    taskDTO.setName(inputName);
	    taskDTO.setDescription(inputDescription);
	    taskDTO.setDateCreated(DateUtil.getDate(new Date().toString()));
	    taskDTO.setDateBegin(dateBegin);
	    taskDTO.setDateEnd(dateEnd);
	    taskDTO.setReadinessStatus(ReadinessStatus.PLANNED);
	    taskEndpoint.taskPersist(sessionDTO, taskDTO);
	    System.out.println("Done");
	    return;
	}

	System.out.println("Task with this name already exist");
    }
}
