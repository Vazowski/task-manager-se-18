package ru.iteco.taskmanager.command.task.remove;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Project;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.api.endpoint.Task;
import ru.iteco.taskmanager.api.endpoint.UserDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.ProjectDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.TaskDTOConvertUtil;

@Singleton
public class TaskRemoveAllCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;
    @Inject
    private IProjectEndpoint projectEndpoint;
    @Inject
    private ITaskEndpoint taskEndpoint;
    @Inject
    private ISessionService sessionService;

    @Override
    public String command() {
        return "task-remove-all";
    }

    @Override
    public String description() {
        return "  -  remove all task in project";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
        if (sessionDTO == null)
            return;
        @Nullable final UserDTO userDTO = userEndpoint.findUserById(sessionDTO, sessionDTO.getUserId());
        if (userDTO == null)
            return;

        System.out.print("Name of project: ");
        @Nullable final String inputProjectName = scanner.nextLine();
        @Nullable final Project tempProject = ProjectDTOConvertUtil
                .DTOToProject(projectEndpoint.findProjectByName(sessionDTO, inputProjectName));
        if (tempProject == null) {
            System.out.println("Project doesn't exist");
            return;
        }
        @Nullable final List<Task> taskList = TaskDTOConvertUtil.DTOsToTasks(taskEndpoint.findAllTask(sessionDTO));
        if (taskList == null) {
            System.out.println("No task");
            return;
        }
        taskEndpoint.removeAllTask(sessionDTO);
        System.out.println("Done");
    }
}
