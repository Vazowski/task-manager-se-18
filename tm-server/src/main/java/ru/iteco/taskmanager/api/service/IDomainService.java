package ru.iteco.taskmanager.api.service;

import ru.iteco.taskmanager.dto.DomainDTO;

public interface IDomainService {

    DomainDTO saveData(final DomainDTO domain) throws Exception;

    DomainDTO loadData() throws Exception;

    DomainDTO jaxbSaveXml(final DomainDTO domain) throws Exception;

    DomainDTO jaxbLoadXml();

    DomainDTO jaxbSaveJson(final DomainDTO domain) throws Exception;

    DomainDTO jaxbLoadJson() throws Exception;

    DomainDTO jacksonSaveXml(final DomainDTO domain) throws Exception;

    DomainDTO jacksonLoadXml() throws Exception;

    DomainDTO jacksonSaveJson(final DomainDTO domain) throws Exception;

    DomainDTO jacksonLoadJson() throws Exception;
}
