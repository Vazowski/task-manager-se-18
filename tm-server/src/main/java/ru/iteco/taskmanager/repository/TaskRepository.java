package ru.iteco.taskmanager.repository;

import java.util.List;

import org.apache.deltaspike.data.api.FullEntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;

import ru.iteco.taskmanager.entity.Task;

@Repository
public interface TaskRepository extends FullEntityRepository<Task, String> {

    @NotNull
    @Query(value = "SELECT a FROM Task a WHERE a.id=:id", max = 1)
    Task findById(@QueryParam("id") @NotNull final String id);

    @NotNull
    @Query(value = "SELECT a FROM Task a WHERE a.name=:name", max = 1)
    Task findByName(@QueryParam("name") @NotNull final String name);

    @NotNull
    @Query(value = "SELECT a FROM Task a WHERE a.project.id=:projectId", max = 1)
    Task findByProjectId(@QueryParam("projectId") @NotNull final String projectId);

    @NotNull
    @Query(value = "SELECT a FROM Task a WHERE a.user.id=:ownerId")
    List<Task> findAllByOwnerId(@QueryParam("ownerId") @NotNull final String ownerId);

    @NotNull
    @Query(value = "SELECT a FROM Task a WHERE name LIKE %:partOfName%")
    List<Task> findAllByPartOfName(@QueryParam("partOfName") @NotNull final String partOfName);

    @NotNull
    @Query(value = "SELECT a FROM Task a WHERE description LIKE %:partOfDescription%")
    List<Task> findAllByPartOfDescription(@QueryParam("partOfDescription") @NotNull final String partOfDescription);
}
