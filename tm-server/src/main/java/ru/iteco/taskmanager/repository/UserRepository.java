package ru.iteco.taskmanager.repository;

import org.apache.deltaspike.data.api.FullEntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.QueryParam;
import org.apache.deltaspike.data.api.Repository;
import org.jetbrains.annotations.NotNull;

import org.jetbrains.annotations.Nullable;
import ru.iteco.taskmanager.entity.User;

@Repository
public interface UserRepository extends FullEntityRepository<User, String> {

    @Nullable
    @Query(value = "SELECT a FROM User a WHERE a.login=:login", max = 1)
    User findByLogin(@QueryParam("login") @NotNull final String login);

    @Nullable
    @Query(value = "SELECT a FROM User a WHERE a.id=:id", max = 1)
    User findById(@QueryParam("id") @NotNull final String id);
}