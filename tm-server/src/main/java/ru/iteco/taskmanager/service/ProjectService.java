package ru.iteco.taskmanager.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.NoResultException;

import lombok.Getter;
import lombok.Setter;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import ru.iteco.taskmanager.api.service.IProjectService;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.repository.ProjectRepository;

@Getter
@Setter
@Singleton
@Transactional
@NoArgsConstructor
public class ProjectService extends AbstractService implements IProjectService {

	@Inject
	private ProjectRepository projectRepository;

    public void merge(@NotNull final Project project) {
    	projectRepository.merge(project);
    }

    public void persist(@NotNull final Project project) {
    	projectRepository.persist(project);
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        try {
            return projectRepository.findById(id);
        } catch (NoResultException e) {
            return null;
        }
    }

    @Nullable
    public Project findByName(@NotNull final String name) {
        try {
            return projectRepository.findByName(name);
        } catch (NoResultException e) {
            return null;
        }
    }

    @Nullable
    public List<Project> findAll() {
    	return projectRepository.findAll();
    }

    @Nullable
    public List<Project> findAllByOwnerId(@NotNull final String ownerId) {
    	return projectRepository.findAllByOwnerId(ownerId);
    }

    @Nullable
    public List<Project> findAllByPartOfName(@NotNull final String partOfName) {
    	return projectRepository.findAllByPartOfName(partOfName);
    }

    @Nullable
    public List<Project> findAllByPartOfDescription(@NotNull final String partOfDescription) {
    	return projectRepository.findAllByPartOfDescription(partOfDescription);
    }

    public void remove(@NotNull final String id) {
    	projectRepository.remove(findById(id));
    }

    public void removeAll() {
		for (Project project : projectRepository.findAll()) {
			projectRepository.remove(project);
		}
	}
}
